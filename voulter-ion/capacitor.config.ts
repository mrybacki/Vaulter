import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'voulter.ion',
  appName: 'voulter-ion',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
